const nodemailer = require("nodemailer");

let transporter = nodemailer.createTransport({
  host: "smtp.gmail.com",
  port: 465,
  secure: true,
  auth: {
    user: process.env.SMTP_USER,
    pass: process.env.SMTP_PWD,
  },
});

const createLead = async (req, res, next) => {
  try {
    const mailToMe = {
      from: "bmrd@email.com",
      to: process.env.SMTP_USER,
      subject: "Nuevo lead en web",
      text: `Nombre: ${req.body.name} | Apellidos: ${
        req.body.lastname
      } | Email: ${req.body.email} | Date: ${new Date()}`,
    };

    constMailToLead = {
      from: process.env.SMTP_USER,
      to: `${req.body.email}`,
      subject: "Thanks for contacting me",
      text: "Hi! Thank you for contact me. I'll write you an email as soon as possible!",
    };
    transporter.sendMail(constMailToLead, (err, info) => {
      console.log("message: ", constMailToLead);
      if (err) {
        console.error(err);
      } else {
        console.log(info);
        res.status(200).json(req.body);
      }
    });
    transporter.sendMail(mailToMe, (err, info) => {
      console.log("message: ", mailToMe);
      if (err) {
        console.error(err);
      } else {
        console.log(info);
        res.status(200).json(req.body);
      }
    });

    res.json({
      status: "Lead created succesfully",
    });
  } catch (error) {
    next(error);
  }
};

module.exports = { createLead };
