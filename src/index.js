require("dotenv").config({ path: ".env" });
const express = require("express");
const app = express();
const emailRouter = require("./routes/email");
const cors = require("cors");
const morgan = require("morgan");
const helmet = require("helmet");

app.use(cors());
app.use(morgan("dev"));
app.use(express.json());
app.use(helmet());

app.use("/leads", emailRouter.router);

app.listen(3000, () => {
  console.log("port running on 3000");
});
