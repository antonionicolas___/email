const mailToMe = {
  from: "bmrd@email.com", //correo de ejemplo
  to: process.env.SMTP_USER,
  subject: "Nuevo lead en web",
  text: `Nombre: ${req.body.name} | Apellidos: ${req.body.lastname} | Email: ${
    req.body.email
  } | Date: ${new Date()}`,
};

const MailToLead = {
  from: process.env.SMTP_USER,
  to: `${req.body.email}`,
  subject: "Thanks for contacting me",
  text: "Hi! Thank you for contact me. I'll write you an email as soon as possible!",
};

module.exports = { mailToMe, MailToLead };
